#!/usr/bin/env bash

# Create partition
sfdisk /dev/sda -uM << EOF
,500
,8000
,100000
;
EOF


# Format partitions
mkfs.fat -F32 /dev/sda1
mkswap /dev/sda2
mkfs.ext4 /dev/sda3
mkfs.ext4 /dev/sda4

# Mount partitions 
